# Infrastructure Engineer Case Study

**NOTE**: Data has been removed from repo due to uncertainty when it comes to security. It need to be copied to the cluster see section **Copy Data to the Cluster** in [Deploy Airflow with Postgres DB](docs/deploy_airflow_on_k8s.md)

## Setup K8s cluster

&nbsp;&nbsp;&nbsp;&nbsp;See: [K8s from scratch on GCE](docs/deploy_k8s_cluster_in_gce.md)

## Get familiar with the data

&nbsp;&nbsp;&nbsp;&nbsp;See: [Data analysis with Jupyter Notebook](data/tinker_with_data.ipynb)

## Design DB

&nbsp;&nbsp;&nbsp;&nbsp;See: [Database design](docs/data_design.md)

## Deploy Airflow with Postgres DB

&nbsp;&nbsp;&nbsp;&nbsp;See: [Deploy Airflow with Postgres DB](docs/deploy_airflow_on_k8s.md)

## Create Data Pipeline in Airflow

&nbsp;&nbsp;&nbsp;&nbsp;See: [Data Pipeline with Airflow](docs/data_pipeline.md)

## Answers to the Questions

&nbsp;&nbsp;&nbsp;&nbsp;See: [Answers](docs/ANSWERS_case_study.md)

