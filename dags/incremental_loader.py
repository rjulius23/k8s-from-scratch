from datetime import datetime, timedelta
import airflow, time
from airflow import DAG
from airflow.hooks.base_hook import BaseHook
from airflow.operators.python_operator import PythonVirtualenvOperator
from airflow.operators.bash_operator import (
    BashOperator,
)  # this module is only necessary if using a BashOperator


default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": airflow.utils.dates.days_ago(1),
    "email": ["airflow@example.com"],  # insert your email here
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}

dag = DAG(
    dag_id="incremental_data_loader",
    default_args=default_args,
    description="Load data regularly",
    schedule_interval="@daily",
)

URI = BaseHook.get_connection("postgres_default").get_uri()


def load_incremental_data(**context):
    uri = context.get("uri", None)
    fname = context.get("fname", None)
    dbname = context.get("dbname", None)

    # Imports
    import os
    import pandas as pd
    from sqlalchemy import create_engine

    # Instantiate sqlachemy.create_engine object
    engine = create_engine(uri)

    df = pd.read_sql(f"SELECT * FROM {dbname} ORDER BY date DESC LIMIT 1", engine)

    print(df.date.values[0])
    latest_entry_date = df.date.values[0]

    files_in_dir = os.listdir("/opt/bitnami/airflow/dags/git/data/incremental_load/")
    files_in_dir.sort()
    idx_of_latest_loaded_file = files_in_dir.index(f"{latest_entry_date}.csv")

    # Process all files with dates that are newer than the last entry's date
    for fname in files_in_dir[idx_of_latest_loaded_file + 1 :]:
        temp_df = pd.read_csv(
            f"/opt/bitnami/airflow/dags/git/data/incremental_load/{fname}",
            names=["storenr", "date", "customers"],
        )
        temp_df.to_sql(dbname, engine, index=False, if_exists="append")


"""
task_1 calls the load_incremental_data function. It will be executed first.
"""

task_1 = PythonVirtualenvOperator(
    task_id="load_incremental_data",
    python_callable=load_incremental_data,
    execution_timeout=timedelta(minutes=3),
    requirements=["psycopg2-binary==2.8.5", "pandas==1.1.1", "SQLAlchemy==1.3.19",],
    dag=dag,
    provide_context=False,
    op_kwargs={"uri": URI, "fname": "customer_number.csv", "dbname": "customer_stats",},
)

task_1
