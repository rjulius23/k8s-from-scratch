from datetime import datetime, timedelta
import airflow, time
from airflow import DAG
from airflow.hooks.base_hook import BaseHook
from airflow.operators.python_operator import PythonVirtualenvOperator
from airflow.operators.bash_operator import (
    BashOperator,
)  # this module is only necessary if using a BashOperator


default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": airflow.utils.dates.days_ago(1),
    "email": ["airflow@example.com"],  # insert your email here
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}

dag = DAG(
    dag_id="initial_data_loader",
    default_args=default_args,
    description="Initialize tables and load initial data",
    schedule_interval="@once",
)

URI = BaseHook.get_connection("postgres_default").get_uri()


def init_db_store_info(**context):
    """Initialize the store_info.
    
    Args:
        context: op_kwargs provided by the Operator. It should contain the DB URI.

    """
    uri = context.get("uri", None)
    import psycopg2

    conn = psycopg2.connect(uri)
    commands = (
        # For testing :)
        """
        DROP TABLE store_info CASCADE
        """,
        """
        DROP TABLE customer_stats CASCADE
        """,
        """
        CREATE TABLE store_info (
            storenr SMALLINT PRIMARY KEY,
            address VARCHAR(255),
            close_date DATE,
            warehousenr SMALLINT NOT NULL
        )
        """,
    )

    try:
        # connect to the PostgreSQL server
        cur = conn.cursor()
        # create table one by one
        for command in commands:
            try:
                cur.execute(command)
            except (Exception, psycopg2.DatabaseError) as err:
                # Dont fail if tables exist already
                if "exist" in str(err):
                    print(err)
                else:
                    raise

        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        raise
    finally:
        if conn is not None:
            conn.close()


def init_db_customer_stats(**context):
    """Initialize the customer_stats.
    
    Args:
        context: op_kwargs provided by the Operator. It should contain the DB URI.

    """
    uri = context.get("uri", None)
    import psycopg2

    conn = psycopg2.connect(uri)
    commands = (
        """ CREATE TABLE customer_stats (
                id SERIAL PRIMARY KEY,
                storenr SMALLINT,
                FOREIGN KEY (storenr)
                  REFERENCES store_info (storenr),
                customers INTEGER NOT NULL,
                date DATE
                )
        """,
    )

    try:
        # connect to the PostgreSQL server
        cur = conn.cursor()
        for command in commands:
            try:
                cur.execute(command)
            except (Exception, psycopg2.DatabaseError) as err:
                # Dont fail if tables exist already
                if "exist" in str(err):
                    print(err)
                else:
                    raise

        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        raise
    finally:
        if conn is not None:
            conn.close()


def load_initial_data(**context):
    """Load initial data into store_info table.
    
    Args:
        context: op_kwargs provided by the Operator.
                 It should contain the DB URI, file name for csv and dbname.

    """
    uri = context.get("uri", None)
    fname = context.get("fname", None)
    dbname = context.get("dbname", None)

    # Imports
    import pandas as pd
    from sqlalchemy import create_engine

    # Load in the data
    df = pd.read_csv(f"/opt/bitnami/airflow/dags/git/data/initial_load/{fname}")

    # Instantiate sqlachemy.create_engine object
    engine = create_engine(uri)

    # Save the data from dataframe to postgres table
    df.to_sql(
        dbname, engine, index=False, if_exists="append"
    )  # Not copying over the index and replace if exists


def validate_initial_data(**context):
    """Validate initial data.
        
    Args:
        context: op_kwargs provided by the Operator.
                 It should contain the DB URI, file name for csv and dbname.

    """
    uri = context.get("uri", None)
    fname = context.get("fname", None)
    dbname = context.get("dbname", None)

    # Imports
    import pandas as pd
    from sqlalchemy import create_engine

    # Instantiate sqlachemy.create_engine object
    engine = create_engine(uri)

    command = f"SELECT * FROM {dbname}"

    print(pd.read_sql(command, engine))


"""
task_0 calls the init_db_store_info function. It will be executed first. If the TABLE is existing it will just silently fail.
TODO: Make it more robust via proper exception handling
"""

task_0 = PythonVirtualenvOperator(
    task_id="init_db_store_info",
    python_callable=init_db_store_info,
    execution_timeout=timedelta(minutes=3),
    requirements=["psycopg2-binary==2.8.5"],
    dag=dag,
    provide_context=False,
    op_kwargs={"uri": URI,},
)

"""
task_1 calls the init_db_customer_stats function. It will be executed first. If the TABLE is existing it will just silently fail.
TODO: Make it more robust via proper exception handling
"""

task_1 = PythonVirtualenvOperator(
    task_id="init_db_customer_stats",
    python_callable=init_db_customer_stats,
    execution_timeout=timedelta(minutes=3),
    requirements=["psycopg2-binary==2.8.5"],
    dag=dag,
    provide_context=False,
    op_kwargs={"uri": URI,},
)

"""
task_2 loads the initial data for store_info table from the csv files under the initial_load folder.
"""

task_2 = PythonVirtualenvOperator(
    task_id="load_initial_data_store",
    python_callable=load_initial_data,
    execution_timeout=timedelta(minutes=3),
    requirements=["psycopg2-binary==2.8.5", "pandas==1.1.1", "SQLAlchemy==1.3.19",],
    dag=dag,
    provide_context=False,
    op_kwargs={"uri": URI, "fname": "stores.csv", "dbname": "store_info"},
)

"""
task_3 loads the initial data for store_info table from the csv files under the initial_load folder.
"""

task_3 = PythonVirtualenvOperator(
    task_id="load_initial_data_cust",
    python_callable=load_initial_data,
    execution_timeout=timedelta(minutes=3),
    requirements=["psycopg2-binary==2.8.5", "pandas==1.1.1", "SQLAlchemy==1.3.19",],
    dag=dag,
    provide_context=False,
    op_kwargs={"uri": URI, "fname": "customer_number.csv", "dbname": "customer_stats",},
)

"""
task_0 validate initial data
"""

task_4 = PythonVirtualenvOperator(
    task_id="validate_initial_data",
    python_callable=validate_initial_data,
    execution_timeout=timedelta(minutes=3),
    requirements=["psycopg2-binary==2.8.5", "pandas==1.1.1", "SQLAlchemy==1.3.19",],
    dag=dag,
    provide_context=False,
    op_kwargs={"uri": URI, "fname": "customer_number.csv", "dbname": "customer_stats",},
)


"""
First create tables, then load initial data and validate them.
We cannot make them parallel due to the foreign key relation.
It may add a bit to the load, however it improves the data integrity significantly.
False data can be filtered out at the loading stage.
"""
task_0 >> task_1 >> task_2 >> task_3 >> task_4


# This is the result of some tinkering around.
#  TODO: Move this code to a separate file, and use the code via KubernetesPodOperator
# class DataOperations:
#     """Class to execute data related operations."""
#
#     def __init__(self, uri):
#         self.uri = uri
#
#     def insert_data(self, storenr: int, date: str, customers: int):
#         """Run query to insert data into DB."""
#         commands = [
#             f"INSERT INTO customer_stats (storenr, date, customers) VALUES({storenr}, {date}, {customers})"
#         ]
#         execute_sql_command(self.uri, commands)
#
#     def execute_sql_command(self, commands: list):
#         """Execute query in SQL DB."""
#         import psycopg2
#
#         try:
#             # connect to the PostgreSQL server
#             conn = psycopg2.connect(self.uri)
#             cur = conn.cursor()
#             # create table one by one
#             for command in commands:
#                 cur.execute(command)
#             # close communication with the PostgreSQL database server
#             cur.close()
#             # commit the changes
#             conn.commit()
#         except (Exception, psycopg2.DatabaseError) as error:
#             print(error)
#         finally:
#             if conn is not None:
#                 conn.close()
#
#     def create_tables(self):
#         """ Create tables in the PostgreSQL database"""
#         commands = (
#             """
#             CREATE TABLE store_info (
#                 storenr SMALLINT PRIMARY KEY,
#                 address VARCHAR(255),
#                 closedate DATE,
#                 warehousenr SMALLINT NOT NULL
#             )
#             """,
#             """ CREATE TABLE customer_stats (
#                     id SERIAL PRIMARY KEY,
#                     FOREIGN KEY (storenr)
#                       REFERENCES store_info (storenr),
#                     customers INTEGER NOT NULL,
#                     date DATE
#                     )
#             """,
#         )
#         self.execute_sql_command(commands)
#
#     def load_store_info(self, **context):
#         pass
#
#
# class InitDb(DataOperations):
#     """Class to initiate DB."""
#
#     @classmethod
#     def execute(cls, **context):
#         db_uri = context.get("uri", None)
#         dbo = cls.super().__init__(db_uri)
#         dbo.create_tables()
