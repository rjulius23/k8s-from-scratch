# Digest Data with Airflow

## Remarks about the Data

Store info contains storenr 277 (Sumeg), however in the initial customer stats there is no entry with that storenr.
In the Initial customer data there is a storenr (805) which is not available inside the initial store info.

This makes violates the data integrity. To fix it there are 2 options:

* Either remove the faulty line from customer_data, however that clearly makes not much sense as that data cme from somewhere.
* Add 805 storenr with Unknown to the store_info. This makes more sense as that store can be just missing from the store registry.
* Remove FK from the customer_stats table, however that way in the future similar issues would be hard to avoid.

Oldest entry in initial data is from '2014-01-02'
Latest entry in initial data is from '2019-04-15'

Data in incremental load and initial load folder overlap from 2019-04-03 till 2019-04-15.

The overlapping data is not the same. Meaning for the same day and storenr value the customer values are different.
With the provided information it is hard to decide which one is the accurate and the mean difference between the values is around 388 customers. On average the difference compared to the initial data is around 4% on April the 3rd.

Hence it makes not much difference statistically to just ignore the discrepancy and go keep the initial data. And only load the incremental data.

The discrepency can come from different reporting methods or processes. (like final correction etc.)

For more details see the Jupyer Notebook: [Data Analysis](../data/tinker_with_data.ipynb)

## Create DAGs

1. Create Initial Loader DAG - designed to run only ONCE to initialize
   1. Initialize store_info table
   2. Initialize customer_stats table
   3. Load store_info initial data from CSV using sqlalchemy and pandas
   4. Load customer_stats initia data from CSV using sqlalchemy and pandas

   ![initial dag](once_dag.PNG)

   **Note**: Steps cannot be paralelized due to the foreign key constraint between store_info and customer_Stats.
   It was a design decision, this way the data integrity can be secured.

2. Create Incremental Loader DAG
   1. Find latest data entry date in Db and load the next days until there are None.

   ![daily dag](daily_dag.PNG)

## Summary view in Airflow

![summary](DAGs.PNG)