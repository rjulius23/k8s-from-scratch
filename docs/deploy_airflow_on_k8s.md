# Deploy Airflow with Postgres DB on K8s

## Install Airflow with BitNami

0. Add the BitNami charts repo to Helm repos
   ```
   helm repo add bitnami https://charts.bitnami.com/bitnami
   ```

1. Deploy Airflow
   ```
   helm install airflow-for-case-study bitnami/airflow \
   --set airflow.cloneDagFilesFromGit.enabled=true \
   --set airflow.cloneDagFilesFromGit.repository=https://gitlab.com/rjulius23/k8s-from-scratch.git \
   --set airflow.cloneDagFilesFromGit.branch=master \
   --set airflow.baseUrl=http://127.0.0.1:8080
   ```

2. Open Port for Airflow
   ```
   kubectl port-forward --namespace default svc/airflow-for-case-study --address 0.0.0.0 8099:8080
   ```

3. Get Postgres DB password
   ```
   kubectl get secret --namespace default airflow-for-case-study-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode
   ```

   Note: postgres container name is airflow-for-case-study-postgresql

4. Configure Postgres Connector via Airflow

   **Conn Id:** postgres_default

   **Schema:** bitnami_airflow

   **Port:** 5432

   **User:** bn_airflow

   **Password:** ```see above```

   **Name:** airflow-for-case-study-postgresql

## Copy Data to the Cluster

For security reasons the data has been removed from the repository hence it need to be copied manually after extracted locally from the received archive:
```
kubectl cp ../data/incremental_load/*.csv airflow-for-case-study-worker-0:/opt/bitnami/airflow/dags/git/data/incremental_load/ -c airflow-worker
```
```
kubectl cp ../data/initial_load/*.csv airflow-for-case-study-worker-0:/opt/bitnami/airflow/dags/git/data/initial_load/ -c airflow-worker
```


