# K8s From Scratch

Build and deploy a K8s cluster on at least 3 a node cluster.

Deploy K8s Cluster with cops, below is based on: [Kops documentation for GCE](https://kops.sigs.k8s.io/getting_started/gce/)

## Installation of K8s Deployment tool (Kops) on Debian/Ubuntu


1. Installing **kops** on server:

    ```curl -Lo kops https://github.com/kubernetes/kops/releases/download/$(curl -s https://api.github.com/repos/kubernetes/kops/releases/latest | grep tag_name | cut -d '"' -f 4)/kops-linux-amd64
    chmod +x ./kops
    sudo mv ./kops /usr/local/bin/
    ```

1. Installing **kubectl**:

    ```
    curl -Lo kubectl https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    chmod +x ./kubectl
    sudo mv ./kubectl /usr/local/bin/kubectl
    ```

1. Installing **gcloud tools**:
   1. Add the Cloud SDK distribution URI as a package source
        ```
        echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
        ```
        Note: you will need apt-transport-https installed as well.

    1. Import the Google Cloud public key
       ```
       curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
       ```

    1. Update and install the Cloud SDK
       ```
       sudo apt-get update && sudo apt-get install google-cloud-sdk
       ```
    
    1. Run to get started
       ```
       gcloud init
       ```

       Note: Authenticate, login and create a new GCP project.
       ```
       gcloud auth application-default login
       ```
    
    1. Confirm everything is working by running:
       ```
       gcloud compute zones list
       ```

       Note: For this to work, you need to enable the GCE API in case of newly created projects.

## Preparation for Cluster deployment

1. Create Service Account for **kops** in **GCP**
   
   Based on:
   https://cloud.google.com/iam/docs/service-accounts#default

   NOTE: Make sure that Compute Admin, Storage Admin rights are provided.

1. Create State Store in GCP
   ```
   gsutil mb gs://kubernetes-clusters-with-airflow/
   ```

   Note: To make it permanent export it inside the .bashrc (*dont forget to source it after*):
   ```
   export KOPS_STATE_STORE=gs://kubernetes-clusters-with-airflow/
   ```

1. Create a VPC for the deployment
   ```
   PROJECT=`gcloud config get-value project`
   gcloud compute networks create k8s-w-airflow-e2e --project=$PROJECT --subnet-mode=auto
   ```
## Create cluster

1. Create cluster configuration with ```kops create cluster```
   ```
   PROJECT=`gcloud config get-value project`
   export KOPS_FEATURE_FLAGS=AlphaAllowGCE # to unlock the GCE features
   kops create cluster simple.k8s.local \
     --cloud gce \
     --zones=europe-west1-b \
     --state ${KOPS_STATE_STORE}/ \
     --image "ubuntu-os-cloud/ubuntu-2004-focal-v20200810" \
     --gce-service-account k8-with-airflow@k8s-w-airflow.iam.gserviceaccount.com \
     --project=${PROJECT}
   ```

1. Confirm that the cluster config is ready
   ```
   kops get cluster
   ```

1. Deploy cluster in GCE
   ```
   kops update cluster simple.k8s.local --yes
   ```