# Infrastructure Engineer Case Study

## Task pin points

- Create a production ready kubernetes cluster
  - You can use local VM-s or any viable solution like public cloud providers
  - Please don't use any managed kubernetes for example EKS, AKS or GKE
  
  **SOLUTION**: [Documented Solution](deploy_k8s_cluster_in_gce.md)
- Deploy a database on the cluster
  - You can use any database type best fit for the study

  **SOLUTION**: [Design DB](data_design.md)
  
- Deploy Airflow on the cluster

  **SOLUTION**: [Deploy Airflow](deploy_airflow_on_k8s.md)

- Use Airflow to load data from the `data/` directory to the database

  **SOLUTION**: [Data pipeline](data_pipeline.md)

## Misc

- Document the steps

  **SOLUTION**: See links above, also markdown files in the docs folder.

- How would you extend your accomplished work further?
  
  **SOLUTION**:
  * Add unit tests, unfortunately I ran out of time and couldnt improve my test coverage.... bad practice.
  * Improve OOP in the Airflow code.
  * Automate steps described in documentation using IaaC tools like Ansible.
  * Create new Helm Charts instead of the Bitnami ones
  * Add new tasks to the DAGs
    * Automate data loading dont rely on manual copy/repo, it should be loaded from and API endpoint
    * Introduce data validation
    * Introduce data cleansing
    * Add notifications, not just email, but Slacks... etc.
  * Improve performance by optimizing the pipelines
    * More Paralellization
    * Look for alternative more optimized solutions
  * Containerize tasks if possible and use KubernetesPodOperator instead of PythonVirtualenvOperator in DAGs.

- Any other improvement you would implement? What would be the next steps once the infrastructure is stable?
  
  **SOLUTION**: 
  * Add monitoring stack to the Kubernetes cluster
    * Introduce Prometheus with cAdvisor at least
  * Scale the cluster to meet the requirements
    * Introduce more powerful VMs into the kubernetes node pool, to improve computation capacity
  * Configure autoscaling to increase capacity and also save costs
  * Look into alternative solutions to Apache Airflow, such as Luigi from Spotify or Prefec Core for example.
- Any comment on the study
  - **It was somewhat challenging, as although I work with Data Scientists their tooling is mainly Jupyter notebooks and Kubernetes cluster with containers. Airflow was a new tool for me, however i feel like i grasped its possibilities. I may not used it to its full potential though, there is definitely more in the tool :). The data itself was tricky, but designing the DB and tinker around in Jupyter notebook helped a lot.**

## Delivery

- Provide a single repository (hosted online or one single archive file) with the whole solution:
  - All the files you created to run the environment
  - Readme file explaining how to run the environment
  - Documentation folder with a document with the answers to the questions above and any other document you need to explain your solution

  **SOLUTION**: [GitLab Repo](https://gitlab.com/rjulius23/k8s-from-scratch.git)
